<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $fillable = ['title', 'description', 'body'];
    protected $hidden = ['created_at', 'updated_at'];
}
